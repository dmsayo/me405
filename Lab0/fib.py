"""@file fib.py
ME405 Lab 0: An interactive Fibonacci number calculator.

https://bitbucket.org/dmsayo/me405/src/master/Lab0/

@author Dominique Sayo
@date 17 April 2020
"""

def fib_num(n):
    """Calculates a Fibonacci number corresponding to
    a specified index n greater than or equal to 0.

    @param n An integer specifying the index n of the desired
               Fibonacci number.
    @returns Fibonacci number Fn.
    @throws ValueError if n is negative.
    """
    if n < 0:
        raise ValueError('Invalid n: {:}'.format(n))

    seq = [0, 1]  # F0 = 0, F1 = 1
    for i in range (2, n + 1, 1): # Calculate Fib sequence up to n
        seq.append(seq[i - 2] + seq[i - 1])  # F(n) = F(n-1) + F(n-2)
    return seq[n]  # Return Fn


if __name__ == '__main__':
    while 1:
        ## Raw user input
        arg = input('Enter index n to calculate Fibonacci number Fn '
                    'or (q) to quit: ')
        ## Input formatted as stripped, lower-case string
        arg_str = str(arg).lower().strip()
        if arg_str[:1] == 'q':  # Exit if first char is 'q'
            quit()

        try:
            ## Input formatted as integer
            arg_int = int(arg)
        except ValueError:  # Unable to convert to int
            print('Error: Enter a valid integer >= 0.')
            continue

        try:
            ## Calculated fibonacci number
            ans = fib_num(arg_int)
            print('Calculating Fibonacci number Fn at '
                'n = {:}.'.format(arg_int))
            print(ans)

        except ValueError:  # Index not valid
            print('Error: Enter a valid integer >= 0.')

