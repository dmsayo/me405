"""@file motor_driver.py
ME405 Lab 1: A class to control motors with the X-NUCLEO-IHM04A1 DC motor driver
expansion board.

https://bitbucket.org/dmsayo/me405/src/master/Lab1/

@package motor
A module for motor control.

@author Dominique Sayo
@date 17 April 2020
"""
import pyb

class Motor:
    """ A motor driver class for the X-NUCLEO-IHM04A1 expansion board.
    """

    def __init__(self, EN_pin, IN1_pin, IN2_pin, timer):
        """ Initialize a motor instance with specified GPIO pins and turn the
        motor off for safety.

        @param EN_pin  A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer   A pyb.Timer object to use for PWM generation on
                       IN1_pin and IN2_pin.
        """
        ## Enable pin of instance
        self.en = EN_pin
        ## IN1 pin of instance
        self.in1 = timer.channel(1, pyb.Timer.PWM, pin=IN1_pin)
        ## IN2 pin of instance
        self.in2 = timer.channel(2, pyb.Timer.PWM, pin=IN2_pin)

        # Turn motors off
        self.in1.pulse_width_percent(0)
        self.in2.pulse_width_percent(0)

    def enable(self):
        """ Enable the motor.
        """
        self.en.high()

    def disable(self):
        """ Disable the motor.
        """
        self.en.low()

    def set_duty(self, duty):
        """ Sets the duty cycle to be sent to the motor to the given level.
        Positive values cause effort in one direction, negative values in the
        opposite direction.

        @param duty A signed integer holding the the duty cycle of the PWM
        signal sent to the motor
        @throws ValueError if duty is not in range [-100, 100]
        """
        if 0 <= duty <= 100:
            self.in1.pulse_width_percent(0)
            self.in2.pulse_width_percent(duty)
        elif -100 <= duty < 0:
            self.in1.pulse_width_percent(-duty)
            self.in2.pulse_width_percent(0)
        else:
            raise ValueError('Invalid duty: {:}'.format(duty))

if __name__=='__main__':
    # Create the pin objects used for interfacing with the two motor driver
    # channels

    ## Channel A EN pin
    pin_EN_A = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    ## Channel A IN1 pin
    pin_IN1_A = pyb.Pin(pyb.Pin.cpu.B4)
    ## Channel A IN2 pin
    pin_IN2_A = pyb.Pin(pyb.Pin.cpu.B5)

    ## Channel B EN pin
    pin_EN_B = pyb.Pin(pyb.Pin.cpu.C1, pyb.Pin.OUT_PP)
    ## Channel B IN1 pin
    pin_IN1_B = pyb.Pin(pyb.Pin.cpu.A0)
    ## Channel B IN2 pin
    pin_IN2_B = pyb.Pin(pyb.Pin.cpu.A1)

    # Create the timer objects used for PWM generation
    ## Channel A Timer
    tim_A = pyb.Timer(3, freq=20000)
    ## Channel B Timer
    tim_B = pyb.Timer(5, freq=20000)

    # Create motor objects passing in the pins and timer
    ## Channel A Motor
    moe_A = Motor(pin_EN_A, pin_IN1_A, pin_IN2_A, tim_A)
    ## Channel B Motor
    moe_B = Motor(pin_EN_B, pin_IN1_B, pin_IN2_B, tim_B)

    # Begin test suite
    print("Testing channel A")
    ## Duty cycle variable
    curr_duty = 20
    moe_A.enable()

    for curr_duty in range(30, -30, -1):
        moe_A.set_duty(curr_duty)
        pyb.delay(100)

    moe_A.disable()
    pyb.delay(750)

    print("Testing channel B")
    moe_B.enable()

    for curr_duty in range(30, -30, -1):
        moe_B.set_duty(curr_duty)
        pyb.delay(100)

    moe_B.disable()

    print("Testing both A and B")
    moe_A.enable()
    moe_B.enable()

    for curr_duty in [-20, 10, 0, -10, 20]:
        moe_A.set_duty(curr_duty)
        moe_B.set_duty(-curr_duty)
        pyb.delay(750)

    moe_A.disable()
    moe_B.disable()

