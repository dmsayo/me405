"""@file encoder.py
ME405 Lab 2: A class to read from a quadrature encoder using a timer.

https://bitbucket.org/dmsayo/me405/src/master/Lab2/

@package encoder
A module for quadrature encoder reading and positional tracking.

@author Dominique Sayo
@date 2 May 2020
"""
import pyb

class Encoder:
    """ A class to monitor motor position via encoder, tracking total positional
    movement with positive and negative direction.
    """

    def __init__(self, A_pin, B_pin, timer):
        """ Initialize an encoder instance with the specified GPIO pins and
        timer.
        @param A_pin A pyb.Pin object to use as the encoder A phase pin. Must be
                     configured to `Pin.IN` mode with alternate function
                     `AF_TIMx`.
        @param B_pin A pyb.Pin object to use as the encoder B phase pin. Must be
                     configured to `Pin.IN` mode with alternate function
                     `AF_TIMx`.
        @param timer A pyb.Timer object to be used in encoder counting mode.
        @throws ValueError if timer period is not `0xFFFF` (for 16-bit timers)
                           or `0x3FFFFFFF` (for 32-bit timers)
        """
        if timer.period() != 0xFFFF and timer.period() != 0x3FFFFFFF:
            raise ValueError("Invalid timer period: 0x%X (must be 0xFFFF or "
                    "0x3FFFFFFF)" % timer.period())

        ## Timer for encoder counting
        self.tim = timer
        ## Timer count
        self.cnt = 0
        ## Previous encoder position
        self.prev_pos = 0
        ## Current encoder position
        self.pos = 0

        ## Configure timer channel 1 and 2 in ENC mode
        ch1 = timer.channel(1, pyb.Timer.ENC_A, pin=A_pin)
        ch2 = timer.channel(2, pyb.Timer.ENC_B, pin=B_pin)

    def __repr__(self):
        """ Object representation for printing instance variables.
        @returns a string representing the encoder object.
        """
        return ("<Encoder object at %s>\n"
                "   tim: %s\n"
                "   cnt: %d\n"
                "   prev_pos: %d\n"
                "   pos: %d\n"
                % (hex(id(self)), str(self.tim), self.cnt,
                   self.prev_pos, self.pos))

    def update(self):
        """ Updates the recorded position of the encoder, accounting for integer
        overflow with the maximum timer value if it occurs.
        """
        prev_cnt = self.cnt                         # Save to previous count
        self.cnt = self.tim.counter()               # Update count

        diff = self.cnt - prev_cnt                  # Calculate difference

        # Overflow detection
        if abs(diff) > (self.tim.period() / 2):     # Difference threshold
            diff = diff & self.tim.period()         # Limit within timer range

            if self.cnt > prev_cnt:                 # If counter rolls under 0
                diff = diff - self.tim.period() - 1 # Subtract max timer val

        self.prev_pos = self.pos                    # Update previous position
        self.pos = self.pos - diff                  # Total pos (sub to match 
                                                    #            pwm direction)

    def get_position(self):
        """ Gets the encoder position.
        @returns the most recently updated position of the encoder.
        """
        return self.pos

    def set_position(self, pos_set=0):
        """ Sets the encoder position.
        @param pos_set A signed integer the encoder position is set to. Defaults
                       to 0 if not specified.
        """
        try:
            self.pos = int(pos_set)
        except ValueError as err:
            print("set_position: Unable to convert argument to int")

    def get_delta(self):
        """ Calculates the difference in encoder position.
        @returns the difference in recorded position between the two most recent
        calls to update().
        """
        return self.pos - self.prev_pos

if __name__=='__main__':
    # Create the pin objects for the encoders
    ## Channel A pin for encoder 1
    pin_A_1 = pyb.Pin(pyb.Pin.cpu.B6, mode=pyb.Pin.IN, af=pyb.Pin.AF2_TIM4)
    ## Channel B pin for encoder 1
    pin_B_1 = pyb.Pin(pyb.Pin.cpu.B7, mode=pyb.Pin.IN, af=pyb.Pin.AF2_TIM4)
    ## Channel A pin for encoder 2
    pin_A_2 = pyb.Pin(pyb.Pin.cpu.C6, mode=pyb.Pin.IN, af=pyb.Pin.AF3_TIM8)
    ## Channel B pin for encoder 2
    pin_B_2 = pyb.Pin(pyb.Pin.cpu.C7, mode=pyb.Pin.IN, af=pyb.Pin.AF3_TIM8)

    # Create the timer objects used for PWM generation
    ## Capture timer for encoder 1 using tim4
    tim_1 = pyb.Timer(4, prescaler=0, period=0xFFFF)
    ## Capture timer for encoder 2 using tim8
    tim_2 = pyb.Timer(8, prescaler=0, period=0xFFFF)

    # Create encoder objects passing in the pins and timer
    ## Instance of encoder 1
    enc_1 = Encoder(pin_A_1, pin_B_1, tim_1)
    ## Instance of encoder 2
    enc_2 = Encoder(pin_A_2, pin_B_2, tim_2)

    # Begin test suite
    ## Reset counter to demonstrate position resets and timer count persists
    rst_cnt = 0
    # Update both encoders every 250 ms and print instance info if changed
    while 1:
        enc_1.update()
        enc_2.update()
        if enc_1.get_delta():
            print(enc_1)
        if enc_2.get_delta():
            print(enc_2)
        pyb.delay(250)
        rst_cnt = rst_cnt + 1
        if rst_cnt == 40:  # Reset every 10 seconds to arbitrary positions
            enc_1.set_position(100)
            enc_2.set_position(-35)
            rst_cnt = 0


