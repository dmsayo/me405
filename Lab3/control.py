"""@file control.py
ME405 Lab 3: A class to perform feedback loop control.

https://bitbucket.org/dmsayo/me405/src/master/Lab3/

@package control
A module for feedback control loops.

@author Dominique Sayo
@date 13 May 2020
"""
import pyb
import utime
from motor_driver import Motor
from encoder import Encoder

def clamp(val, lim):
    """ Clamps a value between upper and lower limits. A limit specified as None
    does not clamp the value.
    @param val The value to be clamped.
    @param lim A tuple representing the (minimum, maximum) values.
    @returns the clamped value.
    """
    lo, hi = lim
    if val is not None:
        if hi is not None and val > hi:
            return hi
        if lo is not None and val < lo:
            return lo
    return val

class Controller:
    """ A class for closed-loop controllers. Calculates an error value between a
    desired setpoint and a measured process variable and applies a correction
    based on (p)roportional, (i)ntegral, and (d)erivative terms.
    """

    def __init__(self, p=None, i=None, d=None, limits=(None, None)):
        """ Initialize a PID controller with corresponding parameters and
        output limits. Any unspecified p, i, or d is defaulted to 0.
        @param p [Optional] A float to be used as the proportional term of the
                 controller. Default is 0.
        @param i [Optional] A float to be used as the integral term of the
                 controller. Default is 0.
        @param d [Optional] A float to be used as the derivative term of the
                 controller. Default is 0.
        @param limits [Optional] A tuple of floats to be used (min, max) limits
                 of the actuation value.
        """
        self.kp = 0           ## Proportional term
        self.ki = 0           ## Integral term
        self.kd = 0           ## Derivative term

        if p is not None:
            self.kp = p
        if i is not None:
            self.ki = i
        if d is not None:
            self.kd = d

        ## (min, max) limits of the output
        self.limits = limits

        ## Running sum for integral calculation
        self.i_sum = 0
        ## Previous recorded time
        self.prev_time = None
        ## Previous recorded process variable
        self.prev_pv = 0

    def __repr__(self):
        """ Object representation for printing instance variables.
        @returns a string representing the controller object.
        """
        return ("<Controller object at %s>\n"
                "   kp: %f\n"
                "   ki: %f\n"
                "   kd: %f\n"
                % (hex(id(self)), self.kp, self.ki, self.kd))

    def update(self, sp, pv):
        """ Calculates actuation value for supplied setpoint and feedback
        process variable.
        @param sp The setpoint or reference.
        @param pv The process variable or the current measured value to be
                  controlled.
        @returns the calculated actuation value.
        """
        now = utime.ticks_ms()
        if self.prev_time is None:
            dt = 0.00001
        elif now - self.prev_time < 0:
            return 0      # Time went backwards?
        else:
            dt = now - self.prev_time

        err = sp - pv           # error value
        dpv = pv - self.prev_pv
        prop = self.kp * err
        self.i_sum += err * dt
        integ = self.ki * self.i_sum
        deriv = self.kd * dpv / dt

        act_val = prop + integ + deriv

        self.prev_time = now
        self.prev_pv = pv
        return clamp(act_val, self.limits)

    def set_k(self, p=None, i=None, d=None):
        """ Set the control parameters kp, ki, and kd. If not specified,
        defaults to 0.
        @param p [Optional] A float to be used as the proportional term of the
                 controller.
        @param i [Optional] A float to be used as the integral term of the
                 controller.
        @param d [Optional] A float to be used as the derivative term of the
                 controller.
        """
        self.kp = 0.0
        self.ki = 0.0
        self.kd = 0.0
        if p is not None:
            self.kp = p
        if i is not None:
            self.ki = i
        if d is not None:
            self.kd = d

    def reset(self):
        """ Resets the internal variables used for integral and derivative
        calculation. 
        """
        self.prev_time = None
        self.prev_pv = 0
        self.i_sum = 0

if __name__=='__main__':
    # Initialize motor on channel A

    ## Channel A EN pin
    pin_EN = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    ## Channel A IN1 pin
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    ## Channel A IN2 pin
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
    ## Channel A Timer
    tim = pyb.Timer(3, freq=20000)
    ## Channel A Motor
    moe = Motor(pin_EN, pin_IN1, pin_IN2, tim)

    # Initialize encoder
    ## Channel A pin for encoder
    pin_A = pyb.Pin(pyb.Pin.cpu.A0, mode=pyb.Pin.IN, af=pyb.Pin.AF1_TIM2)
    ## Channel B pin for encoder
    pin_B = pyb.Pin(pyb.Pin.cpu.A1, mode=pyb.Pin.IN, af=pyb.Pin.AF1_TIM2)

    ## Capture timer for encoder using tim2
    tim = pyb.Timer(2, prescaler=0, period=0xFFFF)
    ## Instance of encoder
    enc = Encoder(pin_A, pin_B, tim)

    # Initialize controller
    ## Instance of PID controller
    ctrl = Controller(p=0.13, limits=(-100, 100))

    while 1:
        ## User input for kp
        p_in = input("\nEnter a value for kp: ")
        try:
            ## Input parsed as float
            ctrl.set_k(p=float(p_in))
        except ValueError as e:
            print("Error: unable to format into a float (%s)" % e)
            continue
        ctrl.reset()
        enc.set_position(0)
        ## Current applied duty cycle
        cur_duty = 0
        ## Current position
        cur_pos = 0
        ## Setpoint position
        setpt_pos = 1000
        ## Array of recorded time values
        time = []
        ## Array of recorded encoder positions
        pos = []
        ## Time zero
        t0 = utime.ticks_ms()

        # Enable motor and run PID loop for 1 second
        moe.enable()
        for i in range(101):
            enc.update()
            cur_pos = enc.get_position()
            cur_duty = ctrl.update(setpt_pos, cur_pos)
            moe.set_duty(cur_duty)
            time.append(utime.ticks_ms() - t0)
            pos.append(cur_pos)
            utime.sleep_ms(10)
        moe.disable()

        # Print values
        for j in range(101):
            print("%d, %d" % (time[j], pos[j]))

