## @file pid_test.py
#  @author Dominique Sayo
#  @date 13 May 2020
#  @page page_pid PID Step Response Tuning
#  Although the control.Controller class supports Kp, Ki, and Kd 
#  terms 
#  for control,
#  only Kp is used in this tuning demonstration for simplicity's sake. To
#  see
#  the step response, a final encoder position of 1000 is used as the setpoint,
#  with the initial position at 0. The motor position is used as the process
#  variable, and the actuation value returned by the PID function is used as the
#  duty cycle supplied to the motor PWM.
#
#  Ideally, the motor would be instantly moved to position 1000, but due to the
#  laws of physics that is not possible. I aimed for a step response without any
#  overshoot that reaches the setpoint in the shortest amount of time. To start,
#  let's use Kp=5.
#
#  ![Step response with p=5](./img/plot_5_0.png)
#
#  Woah, we have some oscilation around the setpoint without it actually
#  stabilising. We can tune it down to Kp=1.
#
#  ![Step response with p=1](./img/plot_1_0.png)
#
#  There is still some oscillation here. It settles eventually, which is good.
#  Let's halve it to Kp=0.5.
#  
#  ![Step response with p=0.5](./img/plot_0_5.png)
#
#  Barely some oscillation left. Maybe halve it again to Kp=0.25.
#
#  ![Step response with p=0.25](./img/plot_0_25.png)
#
#  Ah, the oscillation stopped, but there is some overshoot still. It's getting
#  closer, so let's go to Kp=0.15.
#
#  ![Step response with p=0.15](./img/plot_0_15.png)
#
#  This looks a lot better, but there is a tiny bit of overshoot on the final
#  position. Let's try Kp=0.1.
#
#  ![Step response with p=0.1](./img/plot_0_1.png)
#
#  Hmm, it is undershooting this time. Maybe a happy medium of Kp=0.133?
#
#  ![Step response with p=0.133](./img/plot_0_133.png)
#
#  This looks pretty nice. Maybe there can be optimizations if Ki and
#  Kd are
#  used as well, but for now it'll do. Here is a video of this chosen step
#  response in action.
# 
#  @htmlonly
#  <iframe width="560" height="315"
#  src="https://www.youtube.com/embed/qB4ez5vQOxQ"
#  frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope;
#  picture-in-picture" allowfullscreen></iframe>
#  @endhtmlonly
#
#  And just to explore the low end of Kp values a little more, here is
#  Kp=0.05.
#
#  ![Step response with p=0.05](./img/plot_0_05.png)
#
#  The motor settles at around a position of 900, possibly because the pwm duty
#  cycle is too low to move the motor any further.
#
