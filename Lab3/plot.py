"""@file plot.py
ME405 Lab3: A script to parse and plot step response data.

https://bitbucket.org/dmsayo/me405/src/master/Lab3/

@author Dominique Sayo
@date 13 May 2020
"""
import serial
import matplotlib.pyplot as plt

## kp value of the run
P_VAL = 0.1

if __name__=='__main__':
    # Read data from USB com port
    ## Serial connection
    ser = serial.Serial('/dev/ttyACM1', 115200, timeout=5000)
    ## Array for times
    t = []
    ## Array for positions
    pos = []
    ## Array for ideal step response positions
    step = []

    # Create unit step data
    for i in range(0, 101):
        step.append(1000)

    # Parse incoming serial data
    for j in range(101):
        ## Raw data
        line = ser.readline()
        ## Data parsed into separate strings
        vals = str(line, 'utf-8').strip().split(',')
        t.append(int(vals[0]))
        pos.append(int(vals[1]))

    print(pos)

    # Insert idle data before time 0
    for k in range(-1, -201, -1):
        t.insert(0, k)
        pos.insert(0, 0)
        step.insert(0, 0)

    # Draw plot
    ## Measured step response
    line1, = plt.plot(t, pos, color="green", label="Measured Reponse")
    ## Ideal step response
    line2, = plt.plot(t, step, color= "grey", label="Ideal Response")
    plt.title("Motor Step Response (p=%.2f)" % P_VAL)
    plt.xlabel("Time (ms)")
    plt.ylabel("Encoder position")
    ## Set x min and max values
    plt.xlim(xmin=-200, xmax=1000)
    ## Set y min and max values
    plt.ylim(ymin=0, ymax=1500)
    plt.grid()
    plt.legend()
    plt.show()

