"""@file imu.py
ME405 Lab 4: A class to interface with the BNO055 IMU.

https://bitbucket.org/dmsayo/me405/src/master/Lab4/

@package imu
A module for inertial measurement.

@author Dominique Sayo
@date 20 May 2020
"""
import pyb

# IMU data register addresses
## Operating mode register address (LSB, MSB=+1)
OPR_MODE_ADDR = 0x3D
## Yaw register address
EUL_HEADING_ADDR = 0x1A
## Pitch register address
EUL_PITCH_ADDR = 0x1E
## Roll register address
EUL_ROLL_ADDR = 0x1C
## X-axis angular velocity register address
GYR_DATA_X_ADDR = 0x14
## Y-axis angular velocity register address
GYR_DATA_Y_ADDR = 0x16
## Z-axis anguar velocity register address
GYR_DATA_Z_ADDR = 0x18

# Calibration register addresses
## Accelerometer offset register address (X[0],Y[2],Z[4])
ACC_OFFSET_ADDR = 0x55
## Magnetometer offset register address
MAG_OFFSET_ADDR = 0x5B
## Gyroscope offset register address
GYR_OFFSET_ADDR = 0x61
## Accelerometer radius register address
ACC_RADIUS_ADDR = 0x67
## Magnetometer radius register address
MAG_RADIUS_ADDR = 0x69
## Calibration status register address
CALIB_STAT_ADDR = 0x35

# Operating modes
## Configuration mode
OPR_MODE_CONFIG = 0x00
## Inertial measurement mode
OPR_MODE_IMU = 0x08
## NDOF mode
OPR_MODE_NDOF = 0x0C

class IMU:
    """ A class for communicating with the BNO055 over I2C.
    """

    def __init__(self, bus):
        """ Initialize an IMU object.
        @param bus The number of the STM32 I2C bus to be used.
        @throws OSError if a device (with possible BNO055 addresses 0x28 or
        0x29) is not found on the I2C bus.
        """
        ## Pyb I2C connection
        self.i2c = pyb.I2C(bus, pyb.I2C.MASTER, baudrate=100000)

        # Check if a BNO055 is on the bus
        ## The I2C device address
        self.dev_addr = None
        for addr in self.i2c.scan():
            if (addr == 0x28 or addr == 0x29):
                self.dev_addr = addr
        if self.dev_addr is None:
            raise OSError("Device not found on I2C bus %d" % bus)

    def __repr__(self):
        """ Object representation for printing instance variables.
        @returns a string representing the IMU object.
        """
        return ("<IMU object at %s>\n"
                "   i2c: %s\n"
                "   dev_addr: 0x%2X\n"
                % (hex(id(self)), self.i2c, self.dev_addr))

    def set_mode(self, mode):
        """ Set the IMU to the given mode.
        @param mode The operating mode to be set to. See valid operating mode
        macros OPR_MODE_xxx in imu.py.
        """
        self.i2c.mem_write(mode, self.dev_addr, OPR_MODE_ADDR,
                           timeout=1000, addr_size=8)

    def config_mode(self):
        """ Set the IMU to configuration mode, where all writable registers can
        be written. In this mode, sensor output data is disabled and read as 0.
        """
        self.set_mode(OPR_MODE_CONFIG)

    def IMU_mode(self):
        """ Set the mode to IMU. In this mode, relative orientation in space is
        calculated from accelerometer and gyroscope data and is readily
        available.
        """
        self.set_mode(OPR_MODE_IMU)

    def NDOF_mode(self):
        """ Set the mode to 9 degrees of freedom mode. Absolute orientation data
        is calculated from accelerometer, gyroscope and the magnetometer.
        """
        self.set_mode(OPR_MODE_NDOF)

    def get_orientation(self):
        """ Get the orientation as three Euler angles.
        @returns a tuple of the three Euler angles (yaw, pitch, roll)
        representing orientation in degrees.
        """
        res = ()

        # Read yaw, pitch, and roll registers
        for reg in [EUL_HEADING_ADDR, EUL_PITCH_ADDR, EUL_ROLL_ADDR]:
            bytes = self.i2c.mem_read(2, self.dev_addr, reg,
                                      timeout=1000, addr_size=8)
            val = (bytes[1] << 8) | bytes[0] # Combine bytes
            if val > (0xFFFF / 2):  # Simulate int16_t
                val -= 0xFFFF
            angle = val / 16.0      # Convert to degrees; 1 deg = 16 lsb
            res += (angle,)         # Append to tuple

        return res

    def get_ang_velo(self):
        """ Get the three angular velocities.
        @returns a tuple of the three angular velocities (x, y, z) in degrees
        per second.
        """
        res = ()

        # Read gyroscope x, y, and z registers
        for reg in [GYR_DATA_X_ADDR, GYR_DATA_Y_ADDR, GYR_DATA_Z_ADDR]:
            bytes = self.i2c.mem_read(2, self.dev_addr, reg,
                                      timeout=1000, addr_size=8)
            val = (bytes[1] << 8) | bytes[0]
            if val > (0xFFFF / 2):  # Simulate int16_t
                val -= 0xFFFF
            angle = val / 16.0      # Convert to degrees/second; 1 Dps = 16 lsb
            res += (angle,)         # Append to tuple

        return res

    def get_calibration_data(self):
        """ Get the IMU calibration data.
        @returns a tuple formatted (ACC_OFFSET, MAG_OFFSET, GYR_OFFSET,
        ACC_RADIUS, MAG_RADIUS). xxx_OFFSET are tuples formatted (x, y, z).
        """
        res = ()

        # Read accelerometer, magnetometer, and gyroscope calibration offset
        for reg in [ACC_OFFSET_ADDR, MAG_OFFSET_ADDR, GYR_OFFSET_ADDR]:
            bytes = self.i2c.mem_read(6, self.dev_addr, reg,
                                      timeout=1000, addr_size=8)

            offset = ()
            for i in range(0, 6, 2):
                val = (bytes[i+1] << 8) | bytes[i]
                offset += (val,)

            res += (offset,)

        # Read accelerometer and magnetometer calibration radius
        for reg in [ACC_RADIUS_ADDR, MAG_RADIUS_ADDR]:
            bytes = self.i2c.mem_read(2, self.dev_addr, reg,
                                      timeout=1000, addr_size=8)
            val = (bytes[1] << 8) | bytes[0]
            res += (val,)

        return res

    def get_calibration_status(self):
        """ Get the calibration status.
        @returns a tuple of booleans formatted (sys_calib_stat, gyr_calib_stat,
        acc_calib_stat, mag_calib_stat) representing the calibration statuses of
        the system, gyroscope, accelerometer, and magnetometer respectively.
        """
        res = ()

        # Read calibration status register
        byte = self.i2c.mem_read(1, self.dev_addr, CALIB_STAT_ADDR,
                                 timeout=1000, addr_size=8)
        for shft in range(6, -1, -2):
            stat = (byte[0] >> shft) & 0x03
            if stat == 0x03:
                res += (True,)
            else:
                res += (False,)

        return res

if __name__=='__main__':

    while (1):
        try:
            ## IMU object on I2C bus 3
            imu = IMU(3)
            break  # IMU connected
        except OSError as e:
            # Error: is device on I2C bus 3 and responsive?
            print("%s, trying again in 3 seconds" % (e))
            pyb.delay(3000)

    print(imu)

    imu.NDOF_mode()  # Set to NDOF mode
    print("   Orientation:           Angular Velocity:    Calibrated:")
    print("   Yaw  Pitch  Roll       X     Y     Z        SYS    GYR    ACC    "
          "MAG")
    while 1: # Continuously get calib, orientation, ang. velocity data and print
        ## Calibration status
        cal_stat = imu.get_calibration_status()
        ## Orientation data
        ori = imu.get_orientation()
        ## Angular velocity data
        ang = imu.get_ang_velo()
        print("% 5d % 6d % 5d   % 5d % 5d % 5d        %5s  %5s  %5s  %5s\r" %
                (ori[0], ori[1], ori[2], ang[0], ang[1], ang[2],
                    "{0:<5}".format(repr(cal_stat[0])),
                    "{0:<5}".format(repr(cal_stat[1])),
                    "{0:<5}".format(repr(cal_stat[2])),
                    "{0:<5}".format(repr(cal_stat[3]))), end=' ')

