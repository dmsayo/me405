## @file imu_test.py
#  @author Dominique Sayo
#  @date 20 May 2020
#  @page page_imu_test IMU Testing
#
#  To test inertial measurement, the BNO055 board was rotated along each axis
#  individually while watching the isolated yaw, pitch, and roll values change
#  accordingly. Key angles of 0, 90, 180, and 270 degrees were checked while
#  referencing the angle ranges in Table 3-13 of the [BNO055
#  datasheet](https://cdn-shop.adafruit.com/datasheets/BST_BNO055_DS000_12.pdf).
#
#  ![Table 3-13](./img/table3_13.png)
#
#  Angular velocities were also observed to be close to zero when the board was
#  held still and fluctuating when the board was being rotated. Watch the demo
#  video here:
#  
#  @htmlonly
#  <iframe width="560" height="315"
#  src="https://www.youtube.com/embed/95RG0stobp4"
#  frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope;
#  picture-in-picture" allowfullscreen></iframe>
#  @endhtmlonly
#
