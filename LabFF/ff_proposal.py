## @file ff_proposal.py
#  @author Dominique Sayo
#  @date 19 May 2020
#  @page page_ff_proposal Final Project Proposal
#
#  ### Problem Statement ###
#  A conveyor belt is needed to move small items a short distance. Sometimes
#  items need to be carried back and forth from a higher elevation,
#  so the conveyor belt must have adjustable inclination and speed. Lastly, it
#  must be controllable by a person and have a visual interface for operation.
#
#  ### Project Requirements ###
#  * __Use closed-loop control on at least two actuators__: Both provided DC
#  motors and encoders will be used along with my python PID controller. One
#  motor will be used to control the belt speed, and the other to increase the
#  inclination angle with help from the IMU's orientation data.
#  * __Interface with at least two sensors__: The BNO055 IMU and the encoders on
#  the motors will be used.
#  * __Include at least one new sensor/actuator/software feature__: A Wii
#  Nunchuck will be used as a controller. An LCD panel will be used to show
#  relevant live information such as inclination angle, speed, direction, etc.
#  * __Interact with the environment or a user__: The belt will be controlled
#  via the Nunchuck and it will be capable of moving items placed on it. 
#  * __Use the provided STM32L476RG and MicroPython__: See the BOM below.
#  * __Run in real time__: Embedded software will be developed and used.
#
#  ### Bill of Materials ###
#  ![Bill of Materials](./img/bom.png)
# 
#  All items listed are either supplied in the ME405 lab kit or already owned.
#  Costs are either estimated or based on current market prices of reputable 
#  online merchants such as Digikey.
#  
#  ### Manufacturing and Assembly ###
#  Most of the infrastructure will be built using Lego parts (thanks to my
#  parents for holding onto my childhood toys) and hardware laying
#  around my house. I will try to use a flat bicycle inner tube as the belt
#  itself (or, failing that, duct tape) and use bearings, Lego wheels,
#  rubber bands, and the motors to drive it. Here is a GIF of a quick prototype:
#
#  ![Prototype](./img/prototype.gif)
#
#  Luckily I can use my 3D printer for any needed connectors or parts that I
#  might immediately need. I've already printed an adapter for the motor shaft.
#  I also have heat shrink, connectors, perfboard, etc. if I need to do more
#  elaborate electronics work or wiring jobs.
#
#  ### Safety Assessment ###
#  * Risks from rotating wheels, shafts, and belts can be mitigated by clearing
#  the area of anything that potentially can get snagged such as wires.
#  * Hazards from using equipment like my soldering iron and 3D printer can be
#  avoided by following appropriate procedures such as wearing eye protection,
#  being aware of high temperatures, and turning off equipment when not in use.
#  * In software, motor speeds should have maximums in order to not break
#  plastic parts or dangerously shoot miscellaneous items into the air from the
#  conveyor belt at high speeds.
#  * Lego pieces are choking hazards and can be a distraction. I should not put
#  them in my mouth or get too side-tracked from this project by playing with
#  old Legos.
#  
#  ### Project Timeline ###
#  * __Week 0 (20-24 May)__: Create belt drive prototype and write libraries for
#  Nunchuck and LCD.
#  * __Week 1 (25-31 May)__: Figure out a way to adjust angle using Legos and
#  designing/printing parts if needed.
#  * __Week 2 (1-7 Jun)__: Assemble conveyor belt, integrate components, and
#  devise control system.
#  * __Week 3 (8-12 Jun)__: Tune motors and solidify mechanical/electrical
#  infrastructure (cable management, perfboard, and more Legos and
#  printed parts if needed).
# 
