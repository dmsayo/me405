## @file ff_report.py
#  @author Dominique Sayo
#  @date 12 June 2020
#  @page page_ff_report Final Project Report
#
#  ## Project Summary ###
#  A small conveyor belt with controllable angle and speed was designed to move
#  items. A Wii Nunchuk is used as the controller. Holding the
#  C button and moving the joystick Y axis adjusts belt angle, while holding the
#  Z button and moving the joystick X axis adjusts belt speed/direction. The
#  buttons can be used simultaneously for full control. The live belt status is 
#  shown on a small LCD and
#  persists when the Nunchuk buttons are released after adjustment. Both motors 
#  use closed loop control; the angle motor references the orientation of the
#  IMU attached to the belt frame to maintain the target angle, and the motor
#  that drives the belt references its encoder delta to maintain the target
#  speed. Here is a video demonstration:
#
#  @htmlonly
#  <iframe width="900" height="506.25"
#  src="https://www.youtube.com/embed/j8ehNEQjcrE"
#  frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope;
#  picture-in-picture" allowfullscreen></iframe>
#  @endhtmlonly
#
#  ## System Components ###
#  From the ME405 lab kit, the conveyor belt uses the Nucleo-64 STM32 board, 
#  both motors, the motor driver expansion board, the IMU, and one motor's
#  encoder. Additional components are an LCD panel and a Nintendo Wii Nunchuk
#  controller. I compiled all the datasheets to create the electrical
#  schematic shown below.
#
#  <img src="./img/belt_schematic_margin.png" alt="System Schematic"
#  width="900"/>
#
#  ### LCD ####
#  The STM32 uses GPIO for the LCD driver's control and data bits. 4-bit
#  operation was chosen to reduce the amount of wires and ports used. A specific
#  initialization routine, which is specified in the datasheet, needs
#  to be issued to the LCD before any display commands can be used. A logic
#  analyzer was used to examine the control bits and data bus and verify the
#  implemented commands and delays. See \ref sec_lcd for LCD documentation.
#
#  ### Wii Nunchuk ####
#  The Wii Nunchuk is an I2C device that operates similarly to the IMU or an
#  EEPROM. After a few initialization bytes are sent to the Nunchuk, its
#  data registers continually update and can be read as a user presses buttons
#  or moves the joystick. The onboard accelerometer was not used for this
#  project. See \ref sec_nunchuk for Nunchuk documentation.
#
#  ### 3D Printed Parts ####
#  Parts compatible with Lego Technic axles and rods were 3D printed. Below is
#  the collection of printed components including the motor shafts,
#  motor mounts, worm gear mechanism, IMU mount, and various rails and supports.
#
#  <img src="./img/3d_parts.png" alt="3D Printed Parts"
#  width="900"/>
#
#  ### Belt ####
#  The conveyor belt loop is a cut-up and stapled bicycle inner tube. The motors
#  drive lego wheels connected with rubber bands. One motor rotates a worm gear 
#  to change the angle of the belt.
#
#  <img src="./img/closeup.jpg" alt="Close-up of motors and gears"
#  width="900"/>
#
#  ## Software Architecture ###
#  The flowchart below shows the main loop as well as the PID iteration
#  subroutines.
# 
#  <img src="./img/belt_flowchart_margin.png" alt="Software Flowchart"
#  width="900"/>
#
#  The code snippet below shows the main loop. A few things to note:
#     - The joystick axis values, ranging from approximately [-100, 100], are
#       mapped to their respective angle and speed ranges.
#     - The target speed has a small deadzone because my Nunchuk has a slight 
#       drift and does not always zero perfectly at a neutral position.
#     - The current angle is obtained by taking the negative roll of the IMU
#       because of the way it is mounted to the inclining conveyor belt frame.
#
#  ```py
#    while 1:
#        # Poll Nunchuk
#        chuck.update()
#        if DEBUG:
#            print("% 5d % 5d % 1d % 1d\r" %
#                (chuck.joy_x(), chuck.joy_y(), chuck.btn_c(), chuck.btn_z()),
#                end=' ')
#
#        # Process control data
#        if chuck.btn_c():
#            # Enable angle adjustment
#            target_angle = map_range(chuck.joy_y(), JOY_RANGE, ANGLE_RANGE)
#
#        if chuck.btn_z():
#            # Enable speed adjustment
#            target_speed = map_range(chuck.joy_x(), JOY_RANGE, SPEED_RANGE)
#            if abs(target_speed) < 5:
#                target_speed = 0
#
#        # Run PID on speed
#        enc.update()
#        PID_speed(int(target_speed), enc.get_delta(), speed_ctrl, moe_B)
#
#        # Run PID on angle
#        ori = imu.get_orientation() # Get IMU orientation
#        cur_angle = -ori[2]          # Negative direction of roll
#        PID_angle(int(target_angle), int(cur_angle), angle_ctrl, moe_A)
#
#        LCD_update(lcd, int(target_speed), int(cur_angle))
#  ```
#  ### Source Code Location ####
#  https://bitbucket.org/dmsayo/me405/src/master/LabFF/
#
#  ## Bill of Materials ###
#  The bill of materials was updated with final items and quantities:
#
#  <img src="./img/bom_v2.png" alt="Updated Bill of Materials"
#  width="900"/>
#
#  All items listed are either supplied in the ME405 lab kit or pre-owned.
#  Costs are either estimated or based on current market prices of reputable
#  online merchants such as Digikey.
#
