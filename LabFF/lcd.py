"""@file lcd.py
ME405 Final Project: A class to interface with LCDs with the ST7066U driver,
particularly the NHD-0216HZ in nibble mode.

https://bitbucket.org/dmsayo/me405/src/master/LabFF/lcd.py

@package lcd
A module for controlling the NHD-0216HZ LCD display.

@author Dominique Sayo
@date 20 May 2020
"""
import pyb

# Initialization routine commands specific to : *do not alter*
## First FS command in init routine
FUNCTION_SET1 = 0x3
## Second FS command in init routine
FUNCTION_SET2 = 0x2
## Upper nibble of Display ON/OFF command in init routine
DISPLAY_ON_OFF_CTRL = 0x0
## Upper nibble of display clear command in init routine
DISP_CLEAR1 = 0x0
## Lower nibble of display clear command in init routine
DISP_CLEAR2 = 0x1
## Upper nibble of entry mode set command in init routine
ENTRY_MODE_SET = 0x0

# Initialization options: *for use in init routine*
## N = 1: 2-line display mode
TWO_LINE = 0x8
## N = 0: 1-line display mode
ONE_LINE = 0x0
## F = 1: 5x11 pixel format display mode
FONT_5x11 = 0x4
## F = 0: 5x8 pixel format display mode
FONT_5x8 = 0x0
## D = 1: Display is enabled
DISP_ON = 0xC
## D = 0: Display is disabled
DISP_OFF = 0x8
## C = 1: Cursor is enabled
CURS_ON = 0xA
## C = 0: Cursor is disabled
CURS_OFF = 0x8
## B = 1: Cursor blink is enabled
BLINK_ON = 0x9
## B = 0: Cursor blink is disabled
BLINK_OFF = 0x8
## I/D = 1: Cursor/blink moves right and DDRAM addr += 1
INCREMENT = 0x6
## I/D = 0: Cursor/blink moves left and DDRAM addr -= 1
DECREMENT = 0x4
## S = 1: Entire display is shifted right/left (according to I/D)
SHIFT = 0x5
## S = 0: Entire display is not shifted on write operations
NO_SHIFT = 0x4

class LCD:
    """ A class for the NHD-0216HZ LCD with the ST7066U LCD driver.
    """

    def __init__(self, RS_pin, RW_pin, EN_pin, data_pins):
        """ Initialize an LCD object.
        @param RS_pin A pyb.Pin object to use as the RS pin.
        @param RW_pin A pyb.Pin object to use as the RW pin.
        @param EN_pin A pyb.Pin object to use as the EN pin.
        @param data_pins An array of 4 pyb.Pin objects to use as the data pins.
        """
        ## RS pyb pin
        self.RS_pin = RS_pin
        ## RW pyb pin
        self.RW_pin = RW_pin
        ## EN pyb pin
        self.EN_pin = EN_pin
        ## Four data pins in array
        self.data_pins = data_pins

        # Initialization routine
        pyb.delay(500)
        self.command(FUNCTION_SET1, 40)
        self.command(FUNCTION_SET2, 0)
        self.command(TWO_LINE | FONT_5x8, 40)
        self.command(FUNCTION_SET2, 0)
        self.command(TWO_LINE | FONT_5x8, 40)
        self.command(DISPLAY_ON_OFF_CTRL, 0)
        self.command(DISP_ON | CURS_OFF | BLINK_OFF, 40)
        self.clear()
        self.command(ENTRY_MODE_SET, 0)
        self.command(INCREMENT | NO_SHIFT, 40)

    def __repr__(self):
        """ Object representation for printing instance variables.
        @returns a string representing the LCD object.
        """
        return ("<LCD object at %s>\n"
                "   RS_pin: %s\n"
                "   RW_pin: %s\n"
                "   EN_pin: %s\n"
                "   data_pins: %s\n"
                % (hex(id(self)), self.RS_pin, self.RW_pin, self.EN_pin,
                   self.data_pins))

    def gpio(self, nibble):
        """ Outputs nibble as GPIO.
        @param nibble The nibble to be output to GPIO on data_pins. If the
                      argument is larger than a nibble, the lowest 4 bits will
                      be used.
        """
        for i in range(0, 4):
            self.data_pins[i].value((nibble >> i) & 0x01)

    def pulse_en(self):
        """ Pulses the enable pin.
        """
        self.EN_pin.high()
        pyb.udelay(1)
        self.EN_pin.low()

    def command(self, command, delay):
        """ Executes an LCD command.
        @param command The 4-bit LCD command. If larger than a nibble, the
                       lowest 4 bits will be used.
        @param delay The specified post-command delay in microseconds.
        """
        self.RS_pin.low()
        self.RW_pin.low()
        self.EN_pin.low()
        self.gpio(command)
        self.pulse_en()

        if delay != 0:
            pyb.udelay(delay)

    def clear(self):
        """ Clear the LCD and move cursor to home.
        """
        self.command(DISP_CLEAR1, 0)
        self.command(DISP_CLEAR2, 1600)

    def write_char(self, c):
        """ Write a character to LCD.
        @param c A one-character long string of the character to write.
        @throws ValueError if c is not a one character string.
        """
        self.RS_pin.high()
        self.RW_pin.low()
        self.EN_pin.low()

        if len(c) != 1:
            raise ValueError("Invalid character: %s" % c)

        byte = ord(c)

        self.gpio(byte >> 4)   # Output upper nibble
        self.pulse_en()
        self.gpio(byte & 0x0F) # Output lower nibble
        self.pulse_en()
        pyb.udelay(40)

    def write_str(self, s):
        """ Write a string to the LCD.
        @param s A string to write to the LCD.
        """
        for c in s:
            self.write_char(c)

    def move_cursor(self, loc):
        """ Move the cursor to the respective location according to the DDRAM address.
        @param loc The DDRAM address of the new cursor location.
        """
        self.command(0x8 | ((loc & 0xF0) >> 4), 0)
        self.command(loc & 0x0F, 40)

    def home(self):
        """ Move cursor to top left of the LCD. (DDRAM address 0x00).
        """
        self.move_cursor(0x00)

    def newline(self):
        """ Move cursor to bottom left of the LCD. (DDRAM address 0x40).
        """
        self.move_cursor(0x40)

if __name__=='__main__':
    ## RS pin
    rs = pyb.Pin(pyb.Pin.cpu.B3, pyb.Pin.OUT_PP)
    ## RW pin
    rw = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
    ## EN pin
    en = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
    ## data bit 4
    db_4 = pyb.Pin(pyb.Pin.cpu.H0, pyb.Pin.OUT_PP)
    ## data bit 5
    db_5 = pyb.Pin(pyb.Pin.cpu.H1, pyb.Pin.OUT_PP)
    ## data bit 6
    db_6 = pyb.Pin(pyb.Pin.cpu.C2, pyb.Pin.OUT_PP)
    ## data bit 7
    db_7 = pyb.Pin(pyb.Pin.cpu.C3, pyb.Pin.OUT_PP)

    ## Initialized LCD object
    lcd = LCD(rs, rw, en, [db_4, db_5, db_6, db_7])

    lcd.write_str("Top text")
    lcd.newline()
    lcd.write_str("Bottom text")
