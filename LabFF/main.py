"""@file main.py
ME405 Final Project: A conveyor belt

https://bitbucket.org/dmsayo/me405/src/master/LabFF/main.py

@author Dominique Sayo
@date 13 May 2020
"""
import pyb
import utime
from motor_driver import Motor
from encoder import Encoder
from control import clamp
from control import Controller
from Nunchuk import Nunchuk
from lcd import LCD
from imu import IMU

## Turn console debug output on/off
DEBUG = True

## Maximum belt speed (motor PWM)
MAX_BELT_SPEED = 60
## Maximum belt speed in reverse direction
MIN_BELT_SPEED = -MAX_BELT_SPEED
## Range of belt speeds
SPEED_RANGE = (MIN_BELT_SPEED, MAX_BELT_SPEED)
## Speed threshold to trigger movement
SPEED_THRESH = 5

## Maximum raise speed (motor PWM)
MAX_RAISE_SPEED = 50
## Maximum lower speed
MIN_RAISE_SPEED = -50
## Range of raise/lower speeds
RAISE_SPEED_RANGE = (MIN_RAISE_SPEED, MAX_RAISE_SPEED)

## Maximum belt angle (degrees)
MAX_ANGLE = 45
## Minimum belt angle
MIN_ANGLE = -10
## Range of belt angles
ANGLE_RANGE = (MIN_ANGLE, MAX_ANGLE)

## Maximum joystick value
MAX_JOY = 95
## Minimum joytsick value
MIN_JOY = -104
## Range of joystick values
JOY_RANGE = (MIN_JOY, MAX_JOY)

## The number of bars representing maximum speed on LCD
NUM_BARS = 5
## LCD character code for box
BOX = b'\xFF'
## LCD character code for right arrow
RIGHT_ARROW = b'\x7E'
## LCD character code for left arrow
LEFT_ARROW = b'\x7F'
## LCD cursor location for speed
LCD_SPEED_LOC = 0x07
## LCD cursor location for direction indicator
LCD_DIR_LOC = 0x0E
## LCD cursor location for angle
LCD_ANGLE_LOC = 0x47

def map_range(val, in_range, out_range):
    """ Maps a value from one range to another.
    @param val Number to be mapped from in_range to out_range. Will be clamped
        to in_range before mapping.
    @param in_range A tuple (min, max) of the input range.
    @param out_range A tuple (min, max) of the output range.
    """
    # Clamp val argument to in_range
    val = max(val, in_range[0])
    val = min(val, in_range[1])

    ## Scalar between ranges
    scalar = (out_range[1] - out_range[0]) / (in_range[1] - in_range[0])
    # Calculate using scaled difference from minimum
    return out_range[0] + scalar * (val - in_range[0])

def PID_speed(target_speed, cur_speed, ctrl, moe):
    """ Runs a PID loop iteration on belt speed using the encoder delta.
    @param target_speed Target encoder delta to be used as setpoint.
    @param cur_speed The current encoder delta to be used as the process
                     variable.
    @param ctrl The PID Controller object to be used.
    @param moe The Motor object used to drive the belt.
    """
    cur_duty = ctrl.update(target_speed, cur_speed)
    moe.set_duty(cur_duty)

def PID_angle(target_angle, cur_angle, ctrl, moe):
    """ Runs a PID loop iteration on belt angle using the IMU angle.
    @param target_angle Target IMU angle to be used as setpoint.
    @param cur_angle The current IMU angle to be used as the process variable.
    @param ctrl The PID Controller object to be used.
    @param moe The Motor object used to change the belt angle.
    """
    cur_duty = ctrl.update(target_angle, cur_angle)
    moe.set_duty(cur_duty)

def LCD_update(lcd, target_speed, target_angle):
    """ Updates the LCD with bars representing the belt speed,
    an arrow for direction, and the belt angle.
    @param lcd The LCD object to be updated.
    @param target_speed The speed to be displayed with bars.
    @param target_angle The angle to be displayed as a number.
    """
    # Update speed
    lcd.move_cursor(LCD_SPEED_LOC)
    flag = 0
    val = abs(target_speed) + 1
    for i in range(NUM_BARS):
        val -= (MAX_BELT_SPEED / NUM_BARS)
        if val < 0:
            flag = 1
        if flag:
            lcd.write_char(' ')
        else:
            lcd.write_char(BOX)

    # Update direction
    lcd.move_cursor(LCD_DIR_LOC)
    if target_speed > SPEED_THRESH:
        lcd.write_char(RIGHT_ARROW) # right arrow
    elif target_speed < -SPEED_THRESH:
        lcd.write_char(LEFT_ARROW) # left arrow
    else:
        lcd.write_char(' ')

    # Update angle
    lcd.move_cursor(LCD_ANGLE_LOC)
    if target_angle < 0:
        lcd.write_char('-') # Write negative
    else:
        lcd.write_char(' ') # positive

    if abs(target_angle) >= 10:
        tens = abs(target_angle) // 10
        lcd.write_char(chr(tens + ord('0'))) # Write tens place

    ones = abs(target_angle) % 10
    lcd.write_char(chr(ones + ord('0'))) # Write ones place
    lcd.write_char(' ') # Write to clear tens place if needed

if __name__=='__main__':
    # Initialize LCD
    ## RS pin
    rs = pyb.Pin(pyb.Pin.cpu.B13, pyb.Pin.OUT_PP)
    ## RW pin
    rw = pyb.Pin(pyb.Pin.cpu.B14, pyb.Pin.OUT_PP)
    ## EN pin
    en = pyb.Pin(pyb.Pin.cpu.B15, pyb.Pin.OUT_PP)
    ## data bit 4
    db_4 = pyb.Pin(pyb.Pin.cpu.H0, pyb.Pin.OUT_PP)
    ## data bit 5
    db_5 = pyb.Pin(pyb.Pin.cpu.H1, pyb.Pin.OUT_PP)
    ## data bit 6
    db_6 = pyb.Pin(pyb.Pin.cpu.C2, pyb.Pin.OUT_PP)
    ## data bit 7
    db_7 = pyb.Pin(pyb.Pin.cpu.C3, pyb.Pin.OUT_PP)

    ## Initialized LCD object
    lcd = LCD(rs, rw, en, [db_4, db_5, db_6, db_7])
    # Initialize motor on channel A

    ## Channel A EN pin
    pin_EN_A = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    ## Channel A IN1 pin
    pin_IN1_A = pyb.Pin(pyb.Pin.cpu.B4)
    ## Channel A IN2 pin
    pin_IN2_A = pyb.Pin(pyb.Pin.cpu.B5)

    ## Channel B EN pin
    pin_EN_B = pyb.Pin(pyb.Pin.cpu.C1, pyb.Pin.OUT_PP)
    ## Channel B IN1 pin
    pin_IN1_B = pyb.Pin(pyb.Pin.cpu.A0)
    ## Channel B IN2 pin
    pin_IN2_B = pyb.Pin(pyb.Pin.cpu.A1)

    # Create the timer objects used for PWM generation
    ## Channel A Timer
    tim_A = pyb.Timer(3, freq=20000)
    ## Channel B Timer
    tim_B = pyb.Timer(5, freq=20000)

    # Create motor objects passing in the pins and timer
    ## Channel A Motor
    moe_A = Motor(pin_EN_A, pin_IN1_A, pin_IN2_A, tim_A)
    ## Channel B Motor
    moe_B = Motor(pin_EN_B, pin_IN1_B, pin_IN2_B, tim_B)

    # Initialize encoder for belt drive (moe_B)
    ## Channel A pin for encoder
    pin_A = pyb.Pin(pyb.Pin.cpu.C6, mode=pyb.Pin.IN, af=pyb.Pin.AF3_TIM8)
    ## Channel B pin for encoder
    pin_B = pyb.Pin(pyb.Pin.cpu.C7, mode=pyb.Pin.IN, af=pyb.Pin.AF3_TIM8)
    ## Capture timer for encoder
    tim_enc = pyb.Timer(8, prescaler=0, period=0xFFFF)
    ## Instance of encoder
    enc = Encoder(pin_A, pin_B, tim_enc)
    enc.update() # initial update

    # Initialize IMU
    while (1):
        try:
            ## IMU object on I2C bus 1
            imu = IMU(1)
            break  # IMU connected
        except OSError as e:
            # Error: is device on I2C bus 3 and responsive?
            print("%s, trying again in 3 seconds" % (e))
            pyb.delay(3000)

    imu.NDOF_mode()  # Set IMU to NDOF mode

    # Initialize Nunchuk
    ## Instance of Wii Nunchuk
    chuck = Nunchuk(2)
    if DEBUG:
        print(chuck)
        print("  X   Y   C   Z")

    ## PID controller for speed
    speed_ctrl = Controller(p=-0.25, i=-0.01, limits=SPEED_RANGE)

    ## PID controller for angle
    angle_ctrl = Controller(p=-5, i=-0.001, limits=RAISE_SPEED_RANGE)

    # Enable motors
    moe_A.enable()
    moe_B.enable()

    ## PID target belt speed (%)
    target_speed = 0
    ## PID target belt angle (degrees)
    target_angle = 0

    lcd.write_str("Speed:")
    lcd.newline()
    lcd.write_str("Angle:")

    while 1:
        # Poll Nunchuk
        chuck.update()
        if DEBUG:
            print("% 5d % 5d % 1d % 1d\r" %
                (chuck.joy_x(), chuck.joy_y(), chuck.btn_c(), chuck.btn_z()),
                end=' ')

        # Process control data
        if chuck.btn_c():
            # Enable angle adjustment
            target_angle = map_range(chuck.joy_y(), JOY_RANGE, ANGLE_RANGE)

        if chuck.btn_z():
            # Enable speed adjustment
            target_speed = map_range(chuck.joy_x(), JOY_RANGE, SPEED_RANGE)
            if abs(target_speed) < 5:
                target_speed = 0

        # Run PID on speed
        enc.update()
        PID_speed(int(target_speed), enc.get_delta(), speed_ctrl, moe_B)

        # Run PID on angle
        ## Imu orientation
        ori = imu.get_orientation() # Get IMU orientation
        ## Currently measured belt angle
        cur_angle = -ori[2]          # Negative direction of roll
        PID_angle(int(target_angle), int(cur_angle), angle_ctrl, moe_A)

        LCD_update(lcd, int(target_speed), int(cur_angle))
