"""@file nunchuk.py
ME405 Final Project: A class to interface with the Nintendo Wii Nunchuk.

https://bitbucket.org/dmsayo/me405/src/master/LabFF/nunchuk.py

@package nunchuk
A module for communicating with the Nintendo Wii Nunchuk over I2C.
@author Dominique Sayo
@date 20 May 2020
"""

import pyb

## Option: Disable encryption
NUNCHUK_DISABLE_ENCRYPTION = True
## Data register address
DATA_ADDR = 0x00
## Calibration register address
CALIB_ADDR = 0x20
## Software calibration value X (to correct HW calibration value)
JOY_X_SW_CALIB = -3
## Software calibration value Y (to correct HW calibration value)
JOY_Y_SW_CALIB = -1

class Nunchuk:
    """ A class for communicating with the Nintendo Wii Nunchuk over I2C.
    """

    def __init__(self, bus):
        """ Initialize a Nunchuk object.
        @param bus The number of the STM32 I2C bus to be used.
        """
        ## Pyb I2C connection
        self.i2c = pyb.I2C(bus, pyb.I2C.MASTER, baudrate=100000)
        pyb.delay(1000)

        ## The I2C device address
        self.dev_addr = 0x52

        # Initialization routine
        if NUNCHUK_DISABLE_ENCRYPTION:
            # START 0xF0 0x55 STOP
            self.i2c.send(bytearray([0xF0, 0x55]), self.dev_addr)
            pyb.udelay(500)
            # START 0xFB 0x00 STOP
            self.i2c.send(bytearray([0xFB, 0x00]), self.dev_addr)
            pyb.udelay(500)
        else:
            # START 0x40 0x00 STOP
            self.i2c.send(bytearray([0x40, 0x00]), self.dev_addr)
            pyb.delay(1000)

        ## Calibration data
        self.calib = 14 * []
        self.calib = self.get_calibration()  # Initial calibration

        ## Buffer of control data
        self.data = 6 * []
        self.data = self.update()            # Initial update

    def __repr__(self):
        """ Object representation for printing instance variables.
        @returns a string representing the Nunchuk object.
        """
        return ("<Nunchuk object at %s>\n"
                "   i2c: %s\n"
                "   dev_addr: 0x%2X\n"
                % (hex(id(self)), self.i2c, self.dev_addr))

    def update(self):
        """ Update the 6 bytes of control data from the Nunchuk.
        """
        pyb.udelay(700)
        self.i2c.send(DATA_ADDR, self.dev_addr)
        pyb.udelay(1300)
        self.data = self.i2c.recv(6, self.dev_addr)

    def get_calibration(self):
        """ Read and parse the 16 bytes of calibration data from the Nunchuk.
        """
        pyb.udelay(700)
        self.i2c.send(CALIB_ADDR, self.dev_addr)
        pyb.udelay(1300)
        self.calib = self.i2c.recv(16, self.dev_addr)
        ## Calibration: 0G acceleration (X [9:2])
        self.ACCEL_0G_X = self.calib[0]
        ## Calibration: 0G acceleration (Y [9:2])
        self.ACCEL_0G_Y = self.calib[1]
        ## Calibration: 0G acceleration (Z [9:2])
        self.ACCEL_0G_Z = self.calib[2]
        ## Calibration 0G acceleration (0b00, X [1:0], Y [1:0], Z [1:0])
        self.LSB_0G_XYZ = self.calib[3]
        ## Calibration: 1G acceleration (X [9:2])
        self.ACCEL_1G_X = self.calib[4]
        ## Calibration: 1G acceleration (Y [9:2])
        self.ACCEL_1G_Y = self.calib[5]
        ## Calibration: 1G acceleration (Z [9:2])
        self.ACCEL_1G_Z = self.calib[6]
        ## Calibration 0G acceleration (0b00, X [1:0], Y [1:0], Z [1:0])
        self.LSB_1G_XYZ = self.calib[7]
        ## Calibration maximum joystick value X
        self.JOY_X_MAX = self.calib[8]
        ## Calibration minimum joystick value X
        self.JOY_X_MIN = self.calib[9]
        ## Calibration zero joystick value X
        self.JOY_X_ZERO = self.calib[10]
        ## Calibration maximum joystick value Y
        self.JOY_Y_MAX = self.calib[11]
        ## Calibration minimum joystick value Y
        self.JOY_Y_MIN = self.calib[12]
        ## Calibration zero joystick value Y
        self.JOY_Y_ZERO = self.calib[13]

    def btn_c(self):
        """ Gets the state of the c button.
        @returns True if the c button is pressed and False otherwise.
        """
        if ((~self.data[5] & 0x02) >> 1) == 1:
            return True
        return False

    def btn_z(self):
        """ Gets the state of the z button.
        @returns True if the z button is pressed and False otherwise.
        """
        if (~self.data[5] & 0x01) == 1:
            return True
        return False

    def joy_x(self):
        """ Gets the joystick x-value relative to center. Adjust JOY_X_SW_CALIB
        in nunchuk.py for software calibration.
        @returns an integer representing the joystick x-axis distance from center.
        """
        return self.data[0] - self.JOY_X_ZERO + JOY_X_SW_CALIB

    def joy_y(self):
        """ Gets the joystick y-value relative to center. Adjust JOY_Y_SW_CALIB
        in nunchuk.py for software calibration.
        @returns an integer representing the joystick y-axis distance from center.
        """
        return self.data[1] - self.JOY_Y_ZERO + JOY_Y_SW_CALIB

if __name__=='__main__':

    ## Nunchuk object
    chuck = Nunchuk(2)

    print(chuck)

    print("Joystick X    Joystick Y   C   Z")
    while 1:
        chuck.update()
        print("% 5d % 5d % 1d % 1d\r" %
              (chuck.joy_x(), chuck.joy_y(), chuck.btn_c(), chuck.btn_z()),
              end=' ')
