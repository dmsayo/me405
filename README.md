### ME 405 - Mechatronics

**California Polytechnic State University, San Luis Obispo**

**Spring 2020**

[Dominique Sayo](https://github.com/dsayo)

[Charlie Refvem](https://me.calpoly.edu/faculty/crefvem)

---

#### Labs:
0 *Getting Going*: A Python and git introduction.

1 *Motor Driver*: Drive a motor.

2 *Encoder*: Track movement with a quadrature encoder.

3 *PID Controller*: A PID controller that can be used with a motor and encoder.

4 *Inertial Measurement Unit*: Interface with the BNO055 over I2C.
