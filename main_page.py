## @file main_page.py
#  @author Dominique Sayo
#  @date 13 May 2020
#  @mainpage
#
#  @section sec_intro Introduction
#  Documentation for ME405 labs and projects designed around the
#  [STM32L476RG](https://www.st.com/en/microcontrollers-microprocessors/stm32l476rg.html)
#  development board and
#  [MicroPython](https://docs.micropython.org/en/latest/library/index.html).
#
#  @section sec_mot Motor Driver
#
#  ### Purpose ###
#  A driver to control motors with the
#  [X-NUCLEO-IHM04A1](https://www.st.com/en/ecosystems/x-nucleo-ihm04a1.html)
#  expansion board (with onboard
#  [L6206](https://www.st.com/en/motor-drivers/l6206.html)
#  motor driver) using timer-generated PWM.
#  See motor_driver.Motor and the \ref motor package for implementation details.
#
#  ### Usage ###
#  Construct and initialize a motor object:
#
#       class Motor(EN_pin, IN1_pin, IN2_pin, timer)
#
#     - `EN_pin`: the pin used to enable/disable the motor.
#     - `IN1_pin` and `IN2_pin`: the two pins used for the bridge logic inputs.
#     - `timer`: the timer object used for PWM generation.
#
#  Example usage to initialize a motor:
#  ```py
#  # Using bridge A
#  en = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)  # pin A10 in output mode as EN
#  in1 = pyb.Pin(pyb.Pin.cpu.B4)                  # pin B4 as IN1
#  in2 = pyb.Pin(pyb.Pin.cpu.B5)                  # pin B5 as IN2
#  tim = pyb.Timer(3, freq=20000)                 # use timer 3 at 20 kHz
#
#  mot = Motor(en, in1, in2, tim)                 # create motor object
#  ```
#
#  Pin and timer combinations to drive both bridge channels on the board:
#  Ch.  | L6206 Pin | CPU Pin | Timer | Timer Ch.
#  ---: | --------- | ------- | ----- | ---------
#  A    | EN/OCD    | PA10    | -     | -
#  ^    | IN1       | PB4     | TIM3  | 1
#  ^    | IN2       | PB5     | TIM3  | 2
#  B    | EN/OCD    | PC1     | -     | -
#  ^    | IN1       | PA0     | TIM5  | 1
#  ^    | IN2       | PA1     | TIM5  | 2
#
#
#  Enable the motor and its movement:
#
#       Motor.enable()
#
#  Disable the motor and its movement:
#
#       Motor.disable()
#
#  Set the speed and direction of the motor:
#
#       Motor.set_duty(duty)
#
#     - `duty`: Integer in range [-100, 100]. Greater magnitude drives the motor
#     faster and negative values spin the motor in the opposite direction.
#
#  ### Testing ###
#  `main` in motor_driver.py runs the following test script:
#  1. Create and initialize two motor objects for both bridge channels.
#  2. Enable motor A and sweep duty cycle from positive to negative to test both
#     directions.
#  3. Disable motor A to stop movement.
#  4. Repeat 2. and 3. for motor B.
#  5. Enable both motors and run them simultaneously in opposite directions at
#     various speeds.
#  6. Disable both motors.
#
#  ### Bugs and Limitations ###
#     - As there are only two bridge channels on the expansion board, only up to
#       two motor objects can be created and used simultaneously.
#     - While the L6206 datasheet recommends a switching frequency of 100kHz,
#       this module has been tested with PWM frequencies from 2kHz up to 100kHz
#       with the provided motors. A frequency above the audible frequency
#       (20kHz) may limit unwanted noise.
#
#  ### Source Code Location ###
#  https://bitbucket.org/dmsayo/me405/src/master/Lab1/
#
#
#  @section sec_enc Encoder
#
#  ### Purpose ###
#  A driver for quadrature encoders to measure speed and direction of a rotating
#  shaft (e.g. a motor).
#  See encoder.Encoder and the \ref encoder package for implementation details.
#
#  ### Usage ###
#  Construct and initialize an encoder object:
#
#       class Encoder(A_pin, B_pin, timer)
#
#     - `A_pin`: the pin used for the encoder A phase.
#     - `B_pin`: the pin used for the encoder B phase.
#     - `timer`: the timer object to be used for encoder counting.
#
#  Example usage to initialize an encoder:
#  ```py
#  # Pin B6 as A phase, config as TIM4 input
#  a = pyb.Pin(pyb.Pin.cpu.B6, mode=pyb.Pin.IN, af=pyb.Pin.AF2_TIM4)
#  # Pin B7 as B phase, config as TIM4 input
#  b = pyb.Pin(pyb.Pin.cpu.B7, mode=pyb.Pin.IN, af=pyb.Pin.AF2_TIM4)
#  tim = pyb.Timer(4, prescaler=0, period=0xFFFF) # timer 4; full 16-bit range
#
#  enc = Encoder(a, b, tim)                       # create encoder object
#  ```
#
#  Possible* timer and pin combinations:
#  Timer | CPU Pin | Phase
#  ----- | ------- | -----
#  TIM2  | PA0     | A
#  ^     | PA1     | B
#  TIM4  | PB6     | A
#  ^     | PB7     | B
#  TIM8  | PC6     | A
#  ^     | PC7     | B
#  *More timer to CPU Pin mappings can be found in Table 17 of the
#  [STM32L476xx](https://www.st.com/en/microcontrollers-microprocessors/stm32l476rg.html)
#  datasheet. The timers with quadrature encoder support are TIM1, TIM2, TIM3,
#  TIM4, TIM5, TIM8, and LPTIM1. Use timer channels 1 and 2.
#
#  Update the recorded position of the encoder:
#
#       Encoder.update()
#
#  Get the total position of the encoder:
#
#       Encoder.get_position()
#
#  Set the total position of the encoder (0 if no argument):
#
#       Encoder.set_position(pos_set=0)
#
#     - `pos_set`: Integer to set the total encoder position to. Defaults to 0.
#
#  Get the difference in encoder position between the last two calls to
#  `update()`:
#
#       Encoder.get_delta()
#
#  ### Testing ###
#  `main` in encoder.py runs the following test script:
#  1. Create and initialize two encoder objects, each to track a motor.
#  2. Update both encoders every 250 ms.
#  3. Each time movement is detected (delta is non-zero), print the encoder
#     information including position and timer count.
#  4. Every 10 seconds, reset both encoder positions to an arbitrary non-zero
#     position to test that position resets while timer count persists.
#
#  While the test program runs, physically rotate the motors and watch the
#  console output for correct change in position even in the cases of timer
#  overflow in both directions.
#
#  The usage of TIM2 with period `0x3FFFFFFF` was also tested to ensure that
#  32-bit timers are supported in the case of overflow.
#
#  ### Bugs and Limitations ###
#     - Timers must have quadrature encoder support to be used with
#       this module. The timers with quadrature encoder support are TIM1, TIM2,
#       TIM3, TIM4, TIM5, TIM8, and LPTIM1. Timer channels 1 and 2 must be used.
#     - This module supports the use of both 16-bit and 32-bit timers.
#       16-bit timer period must be `0xFFFF` (TIM1, TIM3, TIM4, TIM8) and
#       32-bit timer period must be `0x3FFFFFFF` (TIM2, TIM5).
#
#  ### Source Code Location ###
#  https://bitbucket.org/dmsayo/me405/src/master/Lab2/
#
#
#  @section sec_ctl PID Controller
#
#  ### Purpose ###
#  A class to run a PID control loop on any system. For this class in
#  particular, encoder equipped motors are controlled as a servo
#  system. for See control.Controller and the
#  \ref control package for implementation
#  details.
#
#  ### Usage ###
#  Construct and initialize a PID controller object:
#
#       class Controller(p=None, i=None, d=None, limits=(None, None))
#
#     - `p`: A float to be used as the proportional term Kp. Defaults to
#            0.
#     - `i`: A float to be used as the integral term Ki. Defaults to 0.
#     - `d`: A float to be used as the derivative term Kd. Defaults to 0.
#     - `limits`: A tuple of floats to be used as the (minimum, maximum)
#                 limits of the actuation value output. If not specified, the
#                 output is not limited.
#
#  Example usage to initialize a PID controller:
#  ```py
#  # Controller with p=0.5, i=0, d=0, limits=(-100, 100)
#  lim = (-100, 100)
#  ctrl1 = Controller(p=0.5, lim)
#
#  # Controller with p=3, i=9, d=0.02, no limits
#  ctrl2 = Controller(p=3, i=9, d=0.02)
#  ```
#
#  Call an update of the PID function, returning an actuation value:
#
#       Controller.update(sp, pv)
#
#     - `sp`: The setpoint or reference.
#     - `pv`: The process variable or the current measured value to be
#             controlled.
#
#  Set the parameters of the PID function:
#
#       Controller.set_k(p=None, i=None, d=None)
#
#     - `p`: Proportional term. Defaults to 0.
#     - `i`: Integral term. Defaults to 0.
#     - `d`: Derivative term. Defaults to 0.
#
#  Reset the stored integral and derivative variables:
#
#       Controller.reset()
#
#  ### Testing ###
#  `main` in encoder.py runs the following test script on the microcontroller:
#  1. Create and initialize motor, encoder, and controller objects.
#  2. Prompt a value for kp.
#  3. Reset variables, arrays, and the controller. Enable the motor.
#  4. For one second, run the PID loop on the motor using a initial position of
#     0 and a setpoint position of 1000 to achieve a step response.
#  5. Disable the motor, and print (time, position) values.
#
#  `main` in plot.py runs the following on a connected PC:
#  1. Read and parse data from serial port the STM32 is connected to.
#  2. Plot the step response on a nice graph using matplotlib.
#
#  See \ref page_pid for tuning methodology and graphs showing the step
#  response for various Kp values.
#
#  ### Bugs and Limitations ###
#     - The motor physically cannot move at low duty cycles, and if the duty
#     cycle actuation value converges to a small value the motor may become
#     stuck at a value that is close but not exactly the target position. Adjust
#     the PID parameters in order to achieve a more accurate response.
#
#  ## Source Code Location ###
#  https://bitbucket.org/dmsayo/me405/src/master/Lab3/
#
#
#  @section sec_imu Inertial Measurement Unit
#
#  ### Purpose ###
#  A class for inertial measurement using the BNO055 sensor. It can measure
#  orientation (yaw, pitch, and roll) and angular velocity on all three axes.
#  See imu.IMU and the
#  \ref imu package for implementation
#  details.
#
#  ### Usage ###
#  Construct and initialize an IMU object:
#
#       class IMU(bus)
#
#     - `bus`: The I2C bus number on the STM32 used for communication.
#
#  Example usage to initialize an IMU (with error checking):
#  ```py
#  # Using I2C bus 3
#  try:
#      imu = IMU(3)
#  except OSError as e:
#      # Error: is device on bus 3 and responsive?
#      print(e)
#  ```
#
#  I2C bus pinout on the STM32 NUCLEO-L476RG:
#  Bus  | Signal | CPU Pin
#  ---- | ------ | -----
#  I2C1 | SCL    | PB6
#  ^    | ^      | PB8
#  ^    | SDA    | PB7
#  ^    | ^      | PB9
#  I2C2 | SCL    | PB10
#  ^    | ^      | PB13
#  ^    | SDA    | PB11
#  ^    | ^      | PB14
#  I2C3 | SCL    | PC0
#  ^    | SDA    | PC1
#
#  Set the operating mode of the IMU:
#
#       IMU.set_mode(mode)
#
#     - `mode`: The operating mode to be set to. See valid operating mode macros
#     OPR_MODE_xxx in imu.py.
#
#  Set the IMU to configuration mode:
#
#       IMU.config_mode()
#
#  Set the IMU to IMU mode, in which the accelerometer and gyro are used:
#
#       IMU.IMU_mode()
#
#  Set the IMU to NDOF mode, in which the accelerometer, magnetometer, and gyro
#  are used.
#
#       IMU.NDOF_mode()
#
#  Get the orientation (yaw, pitch, roll) in degrees:
#
#       IMU.get_orientation()
#
#  Get the angular velocity (x, y, z) in degrees per second:
#
#       IMU.get_ang_velo()
#
#  Get the current calibration status:
#
#       IMU.get_calibration_status()
#
#  Get the current calibration data, including offsets and radius:
#
#       IMU.get_calibration_data()
#
#
#  ### Testing ###
#  `main` in imu.py runs the following test script on the microcontroller:
#  1. Create and initialize and IMU object while checking that it is connected
#  to the I2C bus and is responsive. If not detected, the program will wait
#  until it is.
#  2. Print the object data including the I2C device address and baud rate.
#  4. Set to NDOF mode.
#  5. Continuously read the calibration status, orientation data, and angular
#  velocity data and print it to console.
#
#  See \ref page_imu_test for testing methodology and a short video
#  demonstration.
#
#  ### Bugs and Limitations ###
#     - The BNO055 has much more capability than what is currently
#     supported. Only calibration mode, IMU mode, and the capability to read
#     orientation and angular velocity are implemented, but further development
#     can easily be done by adding appropriate macros and reading from the
#     desired registers while following the existing format. See the [BNO055
#     datasheet](https://cdn-shop.adafruit.com/datasheets/BST_BNO055_DS000_12.pdf)
#     for more device details.
#
#  ## Source Code Location ###
#  https://bitbucket.org/dmsayo/me405/src/master/Lab4/
#
#
#  @section sec_lcd LCD
#
#  ### Purpose ###
#  A class to use LCDs with the ST7066U driver (particularly the NHD-0216HZ)
#  in nibble mode.
#  Links to [LCD datasheet](https://www.newhavendisplay.com/specs/NHD-0216HZ-
#  FSW-FBW-33V3C.pdf) and [driver datasheet](https://www.newhavendisplay.com/
#  app_notes/ST7066U.pdf).
#  See lcd.LCD and the
#  \ref lcd package for implementation
#  details.
#
#  ### Usage ###
#  Construct and initialize an LCD object:
#
#       class LCD(RS_pin, RW_pin, EN_pin, data_pins)
#
#     - `RS_pin`: A pyb.Pin object configured as GPIO output to use as the
#                 Register Select pin.
#     - `RW_pin`: A pyb.Pin object configured as GPIO output to use as the
#                 Read/Write pin.
#     - `EN_pin`: A pyb.Pin object configured as GPIO output to use as the
#                 operation Enable pin.
#     - `data_pins`: An array of 4 pyb.Pin objects configured as GPIO output
#                 to use as the data pins, in the order of bits 4 to 7.
#
#  Example usage to initialize an LCD:
#  ```py
#  rs = pyb.Pin(pyb.Pin.cpu.B3, pyb.Pin.OUT_PP)   # RS pin
#  rw = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)   # RW pin
#  en = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)   # EN pin
#  db_4 = pyb.Pin(pyb.Pin.cpu.H0, pyb.Pin.OUT_PP) # Data bit 4
#  db_5 = pyb.Pin(pyb.Pin.cpu.H1, pyb.Pin.OUT_PP) # Data bit 5
#  db_6 = pyb.Pin(pyb.Pin.cpu.C2, pyb.Pin.OUT_PP) # Data bit 6
#  db_7 = pyb.Pin(pyb.Pin.cpu.C3, pyb.Pin.OUT_PP) # Data bit 7
#
#  # Initialize an LCD object
#  lcd = LCD(rs, rw, en, [db_4, db_5, db_6, db_7])
#  ```
#  Initialization option macros are defined in LCD.py, such as turning the
#  cursor on or off.
#
#  Send a nibble of a command to the LCD (see table of commands in LCD
#  datasheet). This is used to implement the initizliation routine and the
#  following operations, but can be used independently. Use this function
#  twice---once for the upper, then again for the lower nibble of a command:
#
#       LCD.command(command, delay)
#
#     - `command`: The 4-bits of the LCD command to be sent.
#     - `delay`: The specified post-command delay in microseconds. Reference
#                the datasheet for required delays.
#
#  Clear the LCD screen and move cursor to home.
#
#       LCD.clear()
#
#  Write a character to the LCD:
#
#       LCD.write_char(c)
#
#     - `c`: The one-character string to write. A byte string such as
#            ``b'\xFF'`` can be used for direct ASCII representation.
#
#  Write a string to the LCD:
#
#       LCD.write_str(s)
#
#     - `s`: The string to write to the LCD.
#
#  Move the cursor on the LCD:
#
#       LCD.move_cursor(loc)
#
#     - `loc`: the DDRAM address of the new cursor location. From left to
#              right, the top line addresses are 0x00 to 0x0F. The bottom
#              line addresses are 0x40 to 0x4F.
#
#  Move the cursor to home, equivalent to `LCD.move_cursor(0x00)`:
#
#       LCD.home()
#
#  Move the cursor to the bottom line, equivalent to `LCD.move_cursor(0x40)`:
#
#       LCD.newline()
#
#  ### Testing ###
#
#  `main` in lcd.py runs the following test script on the microcontroller:
#  1. Create and initialize an LCD object.
#  2. Write "Top text" on the top line.
#  3. Write "Bottom text" on the bottom line.
#
#  The initialization routine was tested and verified using a logic analyzer.
#  Delays were added and adjusted accordingly.
#
#  ### Bugs and Limitations ###
#     - Only 4-bit mode is used and the LCD must be wired as such; 8-bit mode is
#       not currently implemented.
#     - It is best if the LCD is power cycled before
#       each initialization. If the LCD has already been initialized, reissued
#       commands may cause undefined behavior.
#     - This class can be only be used with other LCDs that use the ST7066U
#       LCD driver without any major modifications. Be sure to change the
#       options in the initialization routine to suit the desired LCD
#       settings.
#
#  ## Source Code Location ###
#  https://bitbucket.org/dmsayo/me405/src/master/LabFF/lcd.py
#
#
#  @section sec_nunchuk Wii Nunchuk
#
#  ### Purpose ###
#  A class to interface with the Nintendo Wii Nunchuk using I2C.
#  See nunchuk.Nunchuk and the
#  \ref nunchuk package for implementation details.
#
#  ### Usage ###
#  Construct and initialize a Nunchuk object:
#
#       class Nunchuk(bus)
#
#     - `bus`: The I2C bus number on the STM32 used for communication.
#
#  Example usage to initialize a Nunchuk:
#  ```py
#  # Using I2C bus 2
#  chuck = Nunchuk(2)
#  ```
#
#  I2C bus pinout on the STM32 NUCLEO-L476RG:
#  Bus  | Signal | CPU Pin
#  ---- | ------ | -----
#  I2C1 | SCL    | PB6
#  ^    | ^      | PB8
#  ^    | SDA    | PB7
#  ^    | ^      | PB9
#  I2C2 | SCL    | PB10
#  ^    | ^      | PB13
#  ^    | SDA    | PB11
#  ^    | ^      | PB14
#  I2C3 | SCL    | PC0
#  ^    | SDA    | PC1
#
#  Poll and update the control data from the Nunchuk:
#
#       Nunchuk.update()
#
#  Get the status of the C button, returning a boolean representing if it is
#  pressed:
#
#       Nunchuk.btn_c()
#
#  Get the status of the Z button, returning a boolean representing if it is
#  pressed:
#
#       Nunchuk.btn_z()
#
#  Get the joystick x-value relative to center:
#
#       Nunchuk.joy_x()
#
#  Get the joystick y-value relative to center:
#
#       Nunchuk.joy_y()
#
#  ### Testing ###
#  `main` in nunchuk.py runs the following test script on the microcontroller:
#  1. Create and initialize an Nunchuk object.
#  2. Print the object data including the I2C device address and baud rate.
#  3. Continuously update the Nunchuk control data by reading the registers,
#     and print the live values to the console.
#
#  The initialization routine and all I2C bus activity was verified using a
#  logic analyzer.
#
#  ### Bugs and Limitations ###
#     - Note that the initialization routine will differ if the Nunchuk is
#       colored black or white. According to online sources,
#       off-brand Nunchuks can be used, but the reliability may not be the
#       same.
#     - There is an option to encrypt the Nunchuk data, which this class does
#       not currently support. As a note: the decode process is (byte ^ 0x17) + 
#       0x17.
#     - The Nunchuk will not work until the correct initialization
#       routine has been sent and received. There is no official
#       documentation on required delays, so some errors that can happen due
#       to a bad initialization routine are: the Nunchuk NACKs write/read
#       requests, the Nunchuk data is read as all 0xFF. The delays work for a
#       (very old) white, OEM Nunchuk used in testing, so try increasing them
#       if the
#       init routine does not work for yours.
#     - The Nunchuk has read-only registers of the calibration values from
#       the time of manufacture, which are used to calculate the joystick
#       positions from center. If the Nunchuk is well-used or miscalibrated
#       for any reason, then adjust JOY_X_HW_CALIB and JOY_Y_HW_CALIB in
#       nunchuk.py for more flexible calibration in software.
#     - The Nunchuk has a built in accelerometer, which this class does not
#       currently support.
#
#  ## Source Code Location ###
#  https://bitbucket.org/dmsayo/me405/src/master/LabFF/nunchuk.py
